#  This file is part of Lazylibrarian.
#
#  Lazylibrarian is free software':'you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Lazylibrarian is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Lazylibrarian.  If not, see <http://www.gnu.org/licenses/>.

import os
import time
import datetime
import traceback

import lazylibrarian
from lazylibrarian import logger, database
from lazylibrarian.common import mime_type, path_exists
# noinspection PyUnresolvedReferences
from six.moves.urllib_parse import unquote_plus

try:
    from rfeed import Item, Guid, Feed, iTunes, iTunesItem, iTunesOwner, iTunesCategory, Enclosure
except ImportError:
    from lib.rfeed import Item, Guid, Feed, iTunes, iTunesItem, iTunesOwner, iTunesCategory, Enclosure
try:
    from lib.tinytag import TinyTag
except ImportError:
    TinyTag = None


def gen_feed(ftype, limit=10, user=0, baseurl='', authorid=None, onetitle=None):
    res = ''
    if not lazylibrarian.CONFIG['RSS_ENABLED']:
        return res
    # noinspection PyBroadException
    try:
        podcast = False
        if ftype == 'eBook':
            cmd = "select AuthorName,BookName,BookDesc,BookLibrary,BookFile,BookID from books,authors where "
            if authorid:
                cmd += 'books.AuthorID = ? and '
            cmd += "BookLibrary != '' and books.AuthorID = authors.AuthorID order by BookLibrary desc limit ?"
            baselink = baseurl + '/book_wall&have=1'
        elif ftype == 'AudioBook':
            podcast = lazylibrarian.CONFIG['RSS_PODCAST']
            cmd = "select AuthorName,BookName,BookSub,BookDesc,AudioLibrary,AudioFile,BookID "
            cmd += "from books,authors where "
            if authorid:
                cmd += 'books.AuthorID = ? and '
            cmd += "AudioLibrary != '' and books.AuthorID = authors.AuthorID "
            cmd += "order by AudioLibrary desc limit ?"
            baselink = baseurl + '/audio_wall'
        elif ftype == 'Magazine':
            cmd = "select Title,IssueDate,IssueAcquired,IssueFile,IssueID from issues "
            if onetitle:
                cmd += 'where Title = ? '
            cmd += "order by IssueAcquired desc limit ?"
            baselink = baseurl + '/mag_wall'
        elif ftype == 'Comic':
            cmd = "select Title,Publisher,comics.ComicID,IssueAcquired,IssueFile,IssueID from comics,comicissues where"
            if onetitle:
                cmd += ' Title = ? and'
            cmd += " comics.comicid=comicissues.comicid order by IssueAcquired desc limit ?"
            baselink = baseurl + '/comic_wall'
        else:
            logger.debug("Invalid feed type")
            return res

        db = database.DBConnection()
        if authorid:
            results = db.select(cmd, (authorid, limit))
        elif onetitle:
            results = db.select(cmd, (unquote_plus(onetitle).replace('&amp;', '&'), limit))
        else:
            results = db.select(cmd, (limit,))
        items = []
        logger.debug("Found %s %s results" % (len(results), ftype))

        if not results:
            podcast = False

        for result in results:
            link = ''
            itunes_item = ''
            if ftype == 'eBook':
                pubdate = datetime.datetime.strptime(result['BookLibrary'], '%Y-%m-%d %H:%M:%S')
                title = result['BookName']
                author = result['AuthorName']
                description = result['BookDesc']
                bookid = result['BookID']
                extn = os.path.splitext(result['BookFile'])[1]
                if user:
                    link = '%s/serve_book/%s%s%s' % (baseurl, user, result['BookID'], extn)

            elif ftype == 'AudioBook':
                pubdate = datetime.datetime.strptime(result['AudioLibrary'], '%Y-%m-%d %H:%M:%S')
                title = result['BookName']
                author = result['AuthorName']
                description = result['BookDesc']
                bookid = result['BookID']
                extn = os.path.splitext(result['AudioFile'])[1]
                if user:
                    link = '%s/serve_audio/%s%s%s' % (baseurl, user, result['BookID'], extn)

                if TinyTag and TinyTag.is_supported(result['AudioFile']) and path_exists(result['AudioFile']):
                    id3r = TinyTag.get(result['AudioFile'])
                    secs = id3r.duration
                    duration = time.strftime('%H:%M:%S', time.gmtime(secs))
                else:
                    duration = "01:11:02"  # any value as default

                itunes_item = iTunesItem(
                    author=result['AuthorName'],
                    image='%s/serve_img/%s%s.jpg' % (baseurl, user, result['BookID']),
                    duration=duration,
                    explicit="clean",
                    subtitle=result['BookSub'],
                    summary=result['BookDesc'])

            elif ftype == 'Magazine':
                pubdate = datetime.datetime.strptime(result['IssueAcquired'], '%Y-%m-%d')
                title = "%s (%s)" % (result['Title'], result['IssueDate'])
                author = result['Title']
                description = title
                bookid = result['IssueID']
                extn = os.path.splitext(result['IssueFile'])[1]
                if user:
                    link = '%s/serve_issue/%s%s%s' % (baseurl, user, result['IssueID'], extn)

            else:  # if ftype == 'Comic':
                pubdate = datetime.datetime.strptime(result['IssueAcquired'], '%Y-%m-%d')
                title = result['Title']
                author = result['Publisher']
                description = title
                bookid = result['IssueID']
                extn = os.path.splitext(result['IssueFile'])[1]
                if user:
                    link = '%s/serve_comic/%s%s_%s%s' % (baseurl, user, result['ComicID'], result['IssueID'], extn)

            if podcast:
                item = Item(
                    title=title,
                    link=link,
                    description=description,
                    author=author,
                    guid=Guid(bookid),
                    pubDate=pubdate,
                    enclosure=Enclosure(url=link, length=0, type=mime_type(result['AudioFile'])),
                    extensions=[itunes_item]
                )
            else:
                item = Item(
                    title=title,
                    link=link,
                    description=description,
                    author=author,
                    guid=Guid(bookid),
                    pubDate=pubdate
                )
            items.append(item)

        itunes = iTunes(
            author="LazyLibrarian",
            subtitle="Podcast of recent audiobooks",
            summary="Audiobooks in the library",
            image='%s/serve_img/%s%s.png' % (baseurl, user, ''),
            explicit="clean",
            categories=iTunesCategory(name='AudioBooks', subcategory='Recent AudioBooks'),
            owner=iTunesOwner(name='LazyLibrarian', email=lazylibrarian.CONFIG['ADMIN_EMAIL']))

        title = "%s Recent Downloads" % ftype
        if authorid and results:
            title = "%s %s Recent Downloads" % (results[0]['AuthorName'], ftype)
        elif onetitle and results:
            title = "%s %s Recent Downloads" % (unquote_plus(onetitle).replace('&amp;', '&'), ftype)

        if podcast:
            feed = Feed(
                title="Podcast rss Feed",
                link=baselink,
                description="LazyLibrarian %s" % title,
                language="en-US",
                lastBuildDate=datetime.datetime.now(),
                items=items,
                extensions=[itunes])
        else:
            feed = Feed(
                title=title,
                link=baselink,
                description="LazyLibrarian %s" % title,
                language="en-US",
                lastBuildDate=datetime.datetime.now(),
                items=items)

        logger.debug("Returning %s %s" % (len(items), ftype))

        res = feed.rss()
    except Exception:
        logger.error('Unhandled exception in rssfeed: %s' % traceback.format_exc())
    return res
