#  This file is part of Lazylibrarian.
#  Lazylibrarian is free software':'you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  Lazylibrarian is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with Lazylibrarian.  If not, see <http://www.gnu.org/licenses/>.

import inspect
import logging
import os
import threading

import lazylibrarian
from lazylibrarian import formatter
from six import PY2


# Simple rotating log handler that uses RotatingFileHandler
class RotatingLogger(object):

    def __init__(self, filename):

        self.filename = filename
        self.filehandler = None
        self.consolehandler = None

    def stop_logger(self):
        lg = logging.getLogger(__name__)
        lg.removeHandler(self.filehandler)
        lg.removeHandler(self.consolehandler)
        self.filehandler = None
        self.consolehandler = None

    def init_logger(self, loglevel=1):

        lg = logging.getLogger(__name__)
        lg.setLevel(logging.DEBUG)

        self.filename = os.path.join(lazylibrarian.CONFIG['LOGDIR'], self.filename)

        # concurrentLogHandler/0.8.7 (to deal with windows locks)
        # since this only happens on windows boxes, if it's nix/mac use the default logger.
        if os.name == 'nt':
            try:
                from lib.concurrent_log_handler import ConcurrentRotatingFileHandler as RotatingFileHandler
                lazylibrarian.LOGTYPE = 'Concurrent'
            except ImportError as e:
                from logging.handlers import RotatingFileHandler
                lazylibrarian.LOGTYPE = 'Rotating (%s)' % e
        else:
            from logging.handlers import RotatingFileHandler
            lazylibrarian.LOGTYPE = 'Rotating'

        filehandler = RotatingFileHandler(
            self.filename,
            maxBytes=lazylibrarian.CONFIG['LOGSIZE'],
            backupCount=lazylibrarian.CONFIG['LOGFILES'])

        filehandler.setLevel(logging.DEBUG)

        fileformatter = logging.Formatter('%(asctime)s - %(levelname)-7s :: %(message)s', '%d-%b-%Y %H:%M:%S')

        filehandler.setFormatter(fileformatter)
        lg.addHandler(filehandler)
        self.filehandler = filehandler

        if loglevel:
            consolehandler = logging.StreamHandler()
            if loglevel == 1:
                consolehandler.setLevel(logging.INFO)
            if loglevel >= 2:
                consolehandler.setLevel(logging.DEBUG)
            consoleformatter = logging.Formatter('%(asctime)s - %(levelname)s :: %(message)s', '%d-%b-%Y %H:%M:%S')
            consolehandler.setFormatter(consoleformatter)
            lg.addHandler(consolehandler)
            self.consolehandler = consolehandler

    @staticmethod
    def log(message, level):

        logger = logging.getLogger(__name__)

        threadname = threading.currentThread().getName()

        # Get the frame data of the method that made the original logger call
        if len(inspect.stack()) > 2:
            frame = inspect.getframeinfo(inspect.stack()[2][0])
            program = os.path.basename(frame.filename)
            method = frame.function
            lineno = frame.lineno
        else:
            program = ""
            method = ""
            lineno = ""

        if os.name == 'nt':  # windows cp1252 can't handle some accents
            message = formatter.unaccented(message)
        elif PY2:
            message = formatter.safe_unicode(message).encode(lazylibrarian.SYS_ENCODING)
        elif not PY2:
            message = message.replace('\x98', '')  # invalid utf-8 eg La mosai\x98que Parsifal

        if level != 'DEBUG' or lazylibrarian.LOGLEVEL >= 2:
            # Limit the size of the "in-memory" log, as gets slow if too long
            lazylibrarian.LOGLIST.insert(0, (formatter.now(), level, threadname, program, method, lineno, message))
            if len(lazylibrarian.LOGLIST) > formatter.check_int(lazylibrarian.CONFIG['LOGLIMIT'], 500):
                del lazylibrarian.LOGLIST[-1]

        message = "%s : %s:%s:%s : %s" % (threadname, program, method, lineno, message)

        if level == 'DEBUG':
            logger.debug(message)
        elif level == 'INFO':
            logger.info(message)
        elif level == 'WARNING':
            logger.warning(message)
        else:
            logger.error(message)


lazylibrarian_log = RotatingLogger('lazylibrarian.log')


def debug(message):
    if lazylibrarian.LOGLEVEL > 1:
        lazylibrarian_log.log(message, level='DEBUG')


def info(message):
    if lazylibrarian.LOGLEVEL > 0:
        lazylibrarian_log.log(message, level='INFO')


def warn(message):
    lazylibrarian_log.log(message, level='WARNING')


def error(message):
    lazylibrarian_log.log(message, level='ERROR')
